#require 'sidekiq/web'

Rails.application.routes.draw do
#  resources :transactxns
#  authenticate :user, lambda { |u| u.admin? } do
#    mount Sidekiq::Web, at: '/sidekiq'
#  end 
#  get 'pages/home'
#  root 'wallets#show'
  root 'pages#home'  
#  get 'pages#home'
  
  resources :wallets, param: :id, only: [:show] do
    resources :transactxns, only: [:index]
    get :deposit, on: :member
    patch :make_deposit, on: :member
    get :withdrawal, on: :member
    patch :make_withdrawal, on: :member
    get :transfer, on: :member
    patch :make_transfer, on: :member
    get :get_funding_sources, on: :member
    get :store_funding_sources, on: :member
    get :check_pending_transactions, on: :member
    resources :funding_sources, except: [:show, :destroy]
  end
  resources :users, only: [:index, :edit, :update] do
    resources :wallets, only: [:new, :create, :destroy, :edit, :update]
  end
  
  get 'wallets/:id/authenticate' => "wallets#authenticate", as: :wallet_authenticate
  get 'wallets/:id/activate' => "wallets#activate", as: :wallet_activate
  post 'notifications/status' => "notifications#transaction_status", as: :transaction_status 
  get 'wallets/:wallet_id/transactxn/:id' => "transactxns#show", as: :show_transaction
  
#  get 'wallets/:id/store_funding_sources' => "wallets#store_funding_sources", as: :store_funding_sources
  
#  get  'wallets/:id/fundingsources' => "funding_sources#index", as: :wallet_funding_sources  
#  post 'wallets/:id/fundingsources' => "funding_sources#create", as: :create_funding_source_wallet
#  get 'wallets/:id/fundingsources/new' => "funding_sources#new", as: :new_funding_source_wallet
#  patch 'wallets/:id/funding_source/verify' => "funding_sources#verify", as: :verify_funding_source_wallet

  devise_for :users
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
