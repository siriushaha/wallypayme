class StatusChangeWorker
  include Sidekiq::Worker

  def perform(transaction_id, status)
    #byebug
    txn = Transactxn.find_by(reference_no: transaction_id.to_s)
    if !txn.nil?      
      wallet = txn.wallet
      txn.status = status
      if txn.deposit?
        case txn.status
          when 'processed'
            wallet.avail_balance += txn.amount
          when 'failed'
            wallet.balance -= txn.amount
            wallet.debits += txn.amount
            wallet.credits -= txn.amount
            txn.txn_type = 'debit'
        end
        update_txn_balances txn, wallet if txn.processed? || txn.failed?     
      elsif txn.withdrawal?
        if txn.failed?
          wallet.avail_balance += txn.amount
          wallet.balance += txn.amount
          wallet.debits -= txn.amount
          wallet.credits += txn.amount
          txn.txn_type = 'credit'
        end
        update_txn_balances txn, wallet if txn.processed? || txn.failed?
      end
      ActiveRecord::Base.transaction do
        raise ActiveRecord::Rollback, 'Transaction update failed' unless wallet.save && txn.save
      end
    end
  end
  
  private
  
  def update_txn_balances(txn, wallet)
    txn.balance = wallet.balance
    txn.avail_balance = wallet.avail_balance
  end
  
end