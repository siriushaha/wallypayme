module UsersHelper
  def wallet_status(user)
    user.wallet.nil? ? 'Not activated' : (user.wallet.active) ? 'Activated' : 'Deactivated'
  end
end