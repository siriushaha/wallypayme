require 'dwolla_api/encryptable'

class WalletDecorator
  include DwollaApi::Encryptable
  
  attr_reader :wallet
  
  def initialize(wallet)
    @wallet = wallet
  end  
  
  def method_missing(method_name, *args, &block)
    wallet.send(method_name, *args, &block)
  end
  
  def respond_to_missing?(method_name, include_private = false)
    wallet.respond_to?(method_name, include_private) || super
  end 

  def display_wallet_id
    WalletDecorator.decrypt(wallet.acct_id) if wallet.acct_id.present?
  end
  
  def available_balance
    
  end
  
end
