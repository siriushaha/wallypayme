class TransactxnDecorator
  
  attr_reader :transactxn
  
  def initialize(transactxn)
    @transactxn = transactxn
  end  
  
  def method_missing(method_name, *args, &block)
    transactxn.send(method_name, *args, &block)
  end
  
  def respond_to_missing?(method_name, include_private = false)
    transactxn.respond_to?(method_name, include_private) || super
  end 

  def display_amount
    transactxn.debit? ? -transactxn.amount : transactxn.amount
  end
  
  def display_status
    status.capitalize if (status == 'failed')
  end
  
end