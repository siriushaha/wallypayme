module Paginable
  extend ActiveSupport::Concern
  
  included do
    TRANSACTION_MAX_PER_PAGE = 10
    USER_MAX_PER_PAGE = 10
  end
  
  private
  
    def pagination_size(max)
      {page: params[:page], per_page: max}
    end
  
end