class TransactxnsController < ApplicationController
  before_action :set_wallet, only: [:index, :show, :update]
  before_action :authenticate_user!, only: [:index, :show, :update]
  before_action :set_transactxn, only: [:show]

  respond_to :html

  def index
    @transactions = @wallet.transactxns.by_not_pending_txns.paginate(pagination_size(Paginable::TRANSACTION_MAX_PER_PAGE))
    @transactions = @transactions.by_type(params[:type]) if params[:type].present?
    if params[:from_date].values.first.present? && params[:to_date].values.first.present?
      from_date = DateTime.parse params[:from_date].values.first
      to_date = DateTime.parse params[:to_date].values.first
      @transactions = @transactions.between_dates(from_date,to_date)
    else
      @transactions = @transactions.from_date(DateTime.parse(params[:from_date].values.first)) if params[:from_date].values.first.present?
      @transactions = @transactions.to_date(DateTime.parse(params[:to_date].values.first)) if params[:to_date].values.first.present?
    end
    @txn_types = Transactxn.txn_types
    @pending_transactions = @wallet.transactxns.by_pending_txns
    #binding.pry
    #logger.info("********* Number of transactions is #{@transactions.size}")
  end
  
  def show
    @transactxn_decorator = TransactxnDecorator.new(@transactxn)
  end
  

  private
    def set_wallet
      @wallet = Wallet.find(params[:wallet_id])
      @wallet_decorator = WalletDecorator.new(@wallet)
    end

    def set_transactxn
      @transactxn = Transactxn.find(params[:id])
    end

    def transactxn_params
      params.require(:transactxn).permit(:type, :amount, :balance, :description)
    end
end
