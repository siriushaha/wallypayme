require 'bigdecimal'
require 'bigdecimal/util'

#require 'dwolla/account_info_scope'

class WalletsController < ApplicationController
  before_action :set_user, only: [:new, :create, :edit, :update]
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :authenticate, :activate, :destroy, :deposit, :make_deposit, :withdrawal, :make_withdrawal, :transfer, :make_transfer, :check_pending_transactions]
  before_action :set_wallet, only: [:show, :edit, :update, :destroy, :authenticate, :activate, :deposit, :make_deposit, :withdrawal, :make_withdrawal, :transfer, :make_transfer, :check_pending_transactions]
  before_action :checkValidTargetWallet, only: [:make_transfer]
  before_action :checkBalanceAvailable, only: [:make_transfer, :make_withdrawal]
#  before_action :set_wallet_state, only: [ :deposit, :withdrawal, :transfer ]
  before_action :set_transfer_transaction, only: [ :transfer ]
  before_action :set_transfer_transaction_with_params, only: [ :make_transfer]
  before_action :set_deposit_withdrawal_transaction, only: [:deposit, :withdrawal]
  before_action :set_deposit_withdrawal_transaction_with_params, only: [:make_deposit, :make_withdrawal]
  before_action :get_funding_sources, only: [:deposit, :withdrawal]
  
  respond_to :html

  def authenticate
    session[:wallet_state] = :update
    wallet_service = WalletService.new(@wallet, flash)
    oauth_url = wallet_service.authenticate wallet_activate_url
    logger.info "******* Dwolla url is #{oauth_url}"
    redirect_to oauth_url
  end

  def activate
    code = wallet_params[:code]
    wallet_service = WalletService.new(@wallet, flash)
    wallet_service.activate code, wallet_activate_url
    redirect_to wallet_path(@wallet)
  end

  def new 
    @wallet = Wallet.new
    session[:wallet_state] = :create
  end
  
  def edit
    session[:wallet_state] = :edit    
  end
  
  def show
    @pending_transactions = @wallet.transactxns.by_pending_txns
    @transactions = @wallet.transactxns.all.by_not_pending_txns.paginate(pagination_size(Paginable::TRANSACTION_MAX_PER_PAGE))
    @txn_types = Transactxn.txn_types
  end
  
  def create
    @wallet = Wallet.new(wallet_params)
    @wallet.user_id = @user.id
    wallet_service = WalletService.new(@wallet, flash)
    if wallet_service.create(@user.email)
      redirect_to users_path 
    else
      render :new
    end
  end

  def update
#    @wallet.active = false
    @wallet.active = params[:wallet][:active]
    @wallet.save
    redirect_to users_path   
  end

  def destroy
  end

  def deposit
    session[:wallet_state] = :deposit
  end
  
  def make_deposit 
    if @wallet_txn.valid?
      deposit_amount = wallet_params[:amount].to_d
      funding_source = FundingSource.find(wallet_params[:funding_source])
      pin = wallet_params[:pin]
      wallet_service = WalletService.new(@wallet, flash)
      begin
        wallet_service.deposit deposit_amount, funding_source, pin
        msg = "Deposit #{number_to_currency(deposit_amount)} from #{funding_source.name.strip}"
        wallet_service.log_info msg
      rescue WalletServiceException
        msg = "Failed to deposit #{number_to_currency(deposit_amount)} to #{funding_source.name.strip}"
        wallet_service.log_warning msg
      end
      redirect_to wallet_path(@wallet)
    else
      get_funding_sources
      render :deposit
    end
  end

  def withdrawal
    session[:wallet_state] = :withdrawal
  end

  def make_withdrawal
    if @wallet_txn.valid?
      withdrawal_amount = wallet_params[:amount].to_d
      funding_source = FundingSource.find(wallet_params[:funding_source])
      pin = wallet_params[:pin]
      wallet_service = WalletService.new(@wallet, flash)
      begin
        wallet_service.withdraw withdrawal_amount, funding_source, pin
        msg = "Withdraw #{number_to_currency(withdrawal_amount)} to #{funding_source.name.strip}"
        wallet_service.log_info msg
      rescue WalletServiceException
        smsg = "Failed to withdraw #{number_to_currency(withdrawal_amount)} from #{funding_source.name.strip}"
        wallet_service.log_warning msg
      end
      redirect_to wallet_path(@wallet)
    else
      get_funding_sources
      render :withdrawal
    end
  end

  def transfer
    session[:wallet_state] = :transfer
  end

  def make_transfer
    if @wallet_txn.valid?
      transfer_amount = wallet_params[:amount].to_d
      pin = wallet_params[:pin]
      notes = wallet_params[:notes]
      wallet_service = WalletService.new(@wallet, flash, @target_wallet)
      begin
        wallet_service.transfer transfer_amount, notes, pin
        msg = "Transfer #{number_to_currency(transfer_amount)} to #{@target_wallet.name.strip}'s Balance"
        wallet_service.log_info msg
      rescue WalletServiceException => e
        msg = "Failed to transfer #{number_to_currency(transfer_amount)} to #{@target_wallet.name.strip}'s Balance due to #{e.message}"
        wallet_service.log_warning msg
      end
      redirect_to wallet_path(@wallet)
    else
      render :transfer
    end
  end

  def check_pending_transactions
    @pending_transactions = @wallet.transactxns.by_pending_txns
    unless @pending_transactions.blank?
      wallet_service = WalletService.new(@wallet, flash)
      wallet_service.check_pending_transactions @pending_transactions 
    end
    redirect_to wallet_path(@wallet)
  end

  private

    def set_user
      @user = User.find(params[:user_id])
    end
  
    def set_wallet
      @wallet = Wallet.find(params[:id])
      @wallet_decorator = WalletDecorator.new(@wallet)
    end

    def set_wallet_state
      session[:wallet_state] = :wallet_transaction   
    end

    def get_funding_sources
      @funding_sources = @wallet.funding_sources.by_external_accounts.verified.select(:id, :name)
    end
    
    def set_transfer_transaction
      @wallet_txn = TransferTransaction.new
    end

    def set_transfer_transaction_with_params
      @wallet_txn = TransferTransaction.new(wallet_params)  
    end

    def set_deposit_withdrawal_transaction
      @wallet_txn = DepositWithdrawalTransaction.new
    end

    def set_deposit_withdrawal_transaction_with_params
      @wallet_txn = DepositWithdrawalTransaction.new(wallet_params)    
    end

    def wallet_params
      if current_user.admin?
        case session[:wallet_state]
          when 'create'
            params.permit(:user_id)
          when 'edit'
            params.permit(:user_id, :id)
          when 'deposit', 'withdrawal'
            params.require(:deposit_withdrawal_transaction).permit(:user_id, :amount, :funding_source, :pin)
          when 'transfer'  
            params.require(:transfer_transaction).permit(:user_id, :amount, :target_email, :pin, :notes)
          else
            params.require(:wallet).permit(:user_id, :name)
        end
      else
        case session[:wallet_state]
          when 'deposit', 'withdrawal'
            params.require(:deposit_withdrawal_transaction).permit(:id, :amount, :funding_source, :pin)
          when 'transfer'  
            params.require(:transfer_transaction).permit(:user_id, :amount, :target_email, :pin, :notes)
          when 'update'
            if (params.key? :code)
              params.permit(:id, :code)
            elsif (params.key? :access_token) && (params.key? :refresh_token)
              params.permit(:access_token, :refresh_token) 
            end
          else
            params.permit(:id) if (params.key? :id)           
        end
      end  
    end

    def checkBalanceAvailable
      redirect_to wallet_path(@wallet), alert: 'Wallet balance is not sufficient to make withdrawal or transfer' unless @wallet.avail_balance >= wallet_params[:amount].to_d
    end

    def checkValidTargetWallet
      target_email = wallet_params[:target_email]
      redirect_to wallet_path(@wallet), alert: 'Transfer not allowed for your own account' unless current_user.email != target_email
      target_user = User.find_by(email: wallet_params[:target_email])
      if target_user.nil? 
        redirect_to wallet_path(@wallet), alert: 'No valid wallet found for this recipient to initiate transfer' 
      else
        if target_user.wallet.nil?
          redirect_to wallet_path(@wallet), alert: 'Wallet for this recipient not activated yet to initiate transfer'
        else
          @target_wallet = target_user.wallet
        end
      end
    end

end
