class UsersController < ApplicationController
  before_action :set_user, only: [:edit, :update]

  respond_to :html
  
  def index
    @users = User.all.paginate(pagination_size(Paginable::USER_MAX_PER_PAGE))
  end

  def edit
  end

  def update
    if @user.update(user_params)
      redirect_to users_path
    else
      render :edit
    end
  end
 
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :admin)
    end
  
    def set_user_recipient
      @user = current_user
    end
  
end
