require 'dwolla'

class NotificationsController < ApplicationController
  before_action :verify_signature, only: [:transaction_status]
  
  def transaction_status
    if params['Type'] == 'Transaction'
      sub_type = params['Subtype']
      status = params['Value']
      transaction_id = params['Id']
      case sub_type
        when 'Status', 'Return'
#          NotificationWorker.perform_async transaction_id, status
        StatusChangeJob.perform_later transaction_id, status
      end
    end
    head :ok
  end
  
  private
  
  def verify_signature
    signature = request.headers["X-Dwolla-Signature"]
    begin
      Dwolla::OffsiteGateway.verify_webhook_signature(signature, request.raw_post)
    rescue => e
      head :unauthorized
    end
  end
  
  def notification_params
    params.permit(:Id, :Type, :Value, :Subtype)
  end
  
end