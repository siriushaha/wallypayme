class FundingSourcesController < ApplicationController
#  before_action :set_user, only: [:new, :create]
  before_action :authenticate_user!, only: [:index, :new, :create, :edit, :update, :destroy]
  before_action :set_wallet, only: [:index, :new, :edit, :create, :update, :destroy]
  before_action :set_create_funding_source, only: [:new]
  before_action :set_verify_funding_source, only: [:edit, :update]
  
  respond_to :html

  def index
    @funding_sources = @wallet.funding_sources.all
    if @funding_sources.nil?
      redirect_to get_funding_sources_wallet_path(@wallet)      
    end
    @funding_sources = @funding_sources.by_external_accounts
    @wallet_decorator = WalletDecorator.new(@wallet)
  end
  
  def new 
    @types = %w(Checking Savings)
    session[:funding_source] = :add
  end
  
  def show
    
  end

  def edit
    session[:funding_source] = :verify
  end

  def create
    @funding_source = CreateFundingSource.new(funding_source_params)
    if @funding_source.valid?
      routing_no = @funding_source.routing_number
      acct_no = @funding_source.account_number
      acct_type = @funding_source.type
      acct_name = @funding_source.name
      wallet_service = WalletService.new(@wallet, flash)
      wallet_service.add_funding_source routing_no, acct_no, acct_type, acct_name
      redirect_to wallet_funding_sources_path(@wallet)
    else
      @types = %w(Checking Savings)
      render :new
    end
  end

  def update
    @funding_source = VerifyFundingSource.new(funding_source_params)
    if @funding_source.valid?
      deposit1 = funding_source_params[:deposit1]
      deposit2 = funding_source_params[:deposit2]
      funding_source_id = @wallet_funding_source.source_id
      wallet_service = WalletService.new(@wallet, flash)
      wallet_service.verify_funding_source funding_source_id, deposit1, deposit2
      redirect_to wallet_funding_sources_path(@wallet)
    else
      @wallet_funding_source = FundingSource.find(params[:id])
      render :edit
    end
  end

  def destroy
    @wallet.active = false
    @wallet.save
    redirect_to wallet_path(@wallet), notice: "Wallet is deactivated."
  end


  private

    def set_wallet
      @wallet = Wallet.find(params[:wallet_id])
      @wallet_decorator = WalletDecorator.new(@wallet)
    end

    def set_create_funding_source
      @funding_source = CreateFundingSource.new
    end
  
    def set_verify_funding_source
      logger.info "Verify funding source id is #{params[:id]}"
      @wallet_funding_source = FundingSource.find(params[:id])
      @funding_source = VerifyFundingSource.new
    end
  
    def funding_source_params
      case session[:funding_source]
        when 'add'
          params.require(:create_funding_source).permit(:name, :type, :routing_number, :account_number)
        when 'verify'  
          params.require(:verify_funding_source).permit(:id, :deposit1, :deposit2)
      end  
    end

end
