#module WallyPay
require 'dwolla_service'
require 'dwolla_api/funding_scope'
require 'dwolla_api/send_scope'
require 'dwolla_api/transaction_scope'
require 'dwolla_api/balance_scope'
require 'dwolla_api/oauth_scope'
require 'dwolla_api/account_info_scope'

  class WalletService
    
    def initialize(source_wallet, flash=nil, target_wallet=nil)
      unless target_wallet.nil?
        @target_wallet = target_wallet 
        @target_dwolla_service = DwollaService.new(@target_wallet)
      end
      @flash = flash unless flash.nil?
      @source_wallet = source_wallet
      @dwolla_service = DwollaService.new(@source_wallet)
    end

    def set_available_balance
      begin
        @dwolla_service.extend DwollaApi::BalanceScope
        @source_wallet.avail_balance = @dwolla_service.balance.to_d
        @source_wallet.save
      rescue DwollaServiceException => e
        raise WalletServiceException, 'Get balance failed'
      end
    end
    
    #Authenticate with Dwolla network
    def authenticate(return_url)
      @dwolla_service.getOAuth return_url      
    end

    #Create wallet info from Dwolla 
    def create(email)
      @dwolla_service.extend DwollaApi::AccountInfoScope
      @source_wallet = @dwolla_service.get_basic_account_info email
      @source_wallet.save 
    end
   
    #Activate wallet with info, funding sources and imported transactions from Dwolla
    def activate(code, return_url)
      begin
        @dwolla_service.store_tokens_to_wallet code, return_url
        @dwolla_service.extend DwollaApi::AccountInfoScope
        @dwolla_service.get_full_account_info
        @dwolla_service.extend DwollaApi::FundingScope
        @dwolla_service.add_funding_sources_to_wallet
        @dwolla_service.extend DwollaApi::TransactionScope
        import_transactions
        @flash[:notice] = "Wallet is activated successfully with funding sources"
      rescue => e
        @flash[:alert] = "Failed to update wallet info due to #{e.message}"
      end      
    end
  
    #Import transactions upon wallet activation
    def import_transactions
      @dwolla_service.extend DwollaApi::TransactionScope
      txns = @dwolla_service.get_transactions
      unless txns.nil?
        txns.sort_by { |txn| txn['Id']}.each { |txn| import_transaction txn}
        @source_wallet.save!
      end
    end

    #Check pending transactions for any status changes
    def check_pending_transactions(pending_txns)
      @dwolla_service.extend DwollaApi::TransactionScope
      pending_txns.each do |pending_txn|
        begin
          txn = @dwolla_service.get_transaction pending_txn.reference_no
#          StatusChangeWorker.perform_async txn['Id'], txn['Status'] unless txn['Status'] == 'pending'
          StatusChangeJob.perform_later txn['Id'], txn['Status'] unless txn['Status'] == 'pending'
        rescue => e
        end
      end      
    end
    
    
    #Add funding source
    def add_funding_source(routing_no, acct_no, acct_type, acct_name)
      begin
        @dwolla_service.extend DwollaApi::FundingScope
        @dwolla_service.add_funding_source_to_wallet routing_no, acct_no, acct_type, acct_name
        flash[:notice] = "New funding source is added successfully."
      rescue DwollaServiceException => e
        flash[:alert] = "Failed to add funding source due to: #{e.message}."
      end        
    end

    #Verify deposit for funding source
    def verify_funding_source(funding_source_id, deposit1, deposit2)
      begin
        @dwolla_service.extend DwollaApi::FundingScope
        @dwolla_service.verify_funding_source_to_wallet funding_source_id, deposit1, deposit2
        flash[:notice] = "Funding source is verified successfully."
      rescue DwollaServiceException => e
        flash[:alert] = "Failed to verify funding source due to #{e.message}."
      end  
    end
    
    #Deposit transaction
    def deposit(amount, funding_source, pin)
      begin
        @dwolla_service.extend DwollaApi::FundingScope
        deposit_transaction = @dwolla_service.deposit_from funding_source.source_id, { amount: amount, pin: pin }
        deposit_txn, @source_wallet = credit_wallet(amount, @source_wallet, "Deposited from #{funding_source.name} account", deposit_transaction['Id'], deposit_transaction['Status'], deposit_transaction['Type'], deposit_transaction['ClearingDate'])
        ActiveRecord::Base.transaction do          
          raise WalletServiceException, 'Deposit transaction failed' unless @source_wallet.save && deposit_txn.save
        end
      rescue DwollaServiceException => e
        raise WalletServiceException, 'Deposit transaction failed' 
      end
    end
  
    #Withdrawal transaction
    def withdraw(amount, funding_source, pin)
      begin
        @source_wallet.avail_balance -= amount
        @dwolla_service.extend DwollaApi::FundingScope
        withdrawal_transaction = @dwolla_service.withdraw_to funding_source.source_id, { amount: amount, pin: pin }
        withdrawal_txn, @source_wallet = debit_wallet(amount, @source_wallet, "Withdrawn to #{funding_source.name} account", withdrawal_transaction['Id'], withdrawal_transaction['Status'], withdrawal_transaction['Type'], withdrawal_transaction['ClearingDate'])
        ActiveRecord::Base.transaction do
          raise WalletServiceException, 'Withdrawal transaction failed' unless @source_wallet.save && withdrawal_txn.save
        end
      rescue DwollaServiceException => e
        raise WalletServiceException, 'Withdrawal transaction failed'
      end
    end

    #Transfer transaction
    def transfer(amount, notes, pin)
      begin
        @dwolla_service.extend DwollaApi::SendScope
        transaction_id = @dwolla_service.send(amount, notes, @target_wallet.acct_id, pin)
        @source_wallet.avail_balance -= amount
        @target_wallet.avail_balance += amount
        @dwolla_service.extend DwollaApi::TransactionScope
        sender_transaction = @dwolla_service.get_transaction transaction_id
        recipient_transaction = @dwolla_service.get_transaction transaction_id, @target_wallet
        withdrawal_txn, @source_wallet = debit_wallet(amount, @source_wallet, "Sent to #{@target_wallet.name.strip}'s Balance", sender_transaction['Id'], sender_transaction['Status'], sender_transaction['Type'], nil, sender_transaction['Notes'])
        deposit_txn, @target_wallet = credit_wallet(amount, @target_wallet, "Received from #{@source_wallet.name.strip}'s Balance", recipient_transaction['Id'], recipient_transaction['Status'], recipient_transaction['Type'], nil, sender_transaction['Notes'])
        ActiveRecord::Base.transaction do
          raise WalletServiceException, 'Transfer transaction failed' unless @source_wallet.save && withdrawal_txn.save && @target_wallet.save && deposit_txn.save
        end
      rescue DwollaServiceException => e
        raise WalletServiceException, e.message
      end
    end
    
    #Charge fee transaction
    def charge_fee(amount, charge_description)
      charge_fee_txn, @source_wallet = debit_wallet(amount, @source_wallet, charge_description)
      ActiveRecord::Base.transaction do
        raise WalletServiceException, 'Charge fee transaction failed' unless @source_wallet.save && charge_fee_txn.save
      end
    end      
    
    #log info messages
    def log_info(msg)
      @flash[:notice] = msg
      log_message :notice, msg
    end
    
    #log warning messages
    def log_warning(msg)
      @flash[:alert] = msg
      log_message :alert, msg
    end
    
    private
    
      def credit_wallet(amount, target_wallet, description, reference_no, status, txn_is_a_type_of, clearing_date=nil, notes=nil)
        target_wallet.balance += amount
        target_wallet.credits += amount
        credit_txn = create_transaction(amount, description, Transactxn.txn_types[:credit], reference_no, status, txn_is_a_type_of, clearing_date, notes, target_wallet)
        return credit_txn, target_wallet
      end
    
      def debit_wallet(amount, source_wallet, description, reference_no, status, txn_is_a_type_of,clearing_date=nil, notes=nil)
        source_wallet.balance -= amount
        source_wallet.debits += amount        
        debit_txn = create_transaction(amount, description, Transactxn.txn_types[:debit], reference_no, status, txn_is_a_type_of, clearing_date, notes, source_wallet)
        return debit_txn, source_wallet
      end
    
      def create_transaction(amount, description, txn_type, reference_no, status, txn_is_a_type_of, clearing_date, notes, wallet)
        txn = Transactxn.new
        txn.amount = amount
        txn.balance = wallet.balance
        txn.avail_balance = wallet.avail_balance
        txn.description = description
        txn.txn_type = txn_type
        txn.reference_no = reference_no
        txn.status = status
        txn.txn_is_a_type_of = txn_is_a_type_of
        txn.wallet_id = wallet.id
        txn.clearing_date = DateTime.parse(clearing_date) if clearing_date.present?
        txn.notes = notes if notes.present?
        txn
      end
    
      def import_transaction(transaction)
        txn = Transactxn.new
        txn.amount = transaction['Amount'].to_d
        txn.reference_no = transaction['Id']
        txn.created_at = txn.updated_at = DateTime.parse(transaction['Date'])
        txn.txn_is_a_type_of = transaction['Type']
        txn.status = transaction['Status']
        txn.clearing_date = DateTime.parse transaction['ClearingDate'] if txn.deposit? || txn.withdrawal?
        txn.notes = transaction['Notes'] if txn.money_received? || txn.money_sent?
        if txn.deposit?
          txn.description = "Deposited from #{transaction['SourceName']}"
          if txn.pending? || txn.processed?
            txn.txn_type = 'credit'
            @source_wallet.credits += txn.amount
            @source_wallet.balance += txn.amount
            @source_wallet.avail_balance = @source_wallet.balance if txn.processed?
          elsif txn.failed?
            txn.txn_type = 'debit'
            @source_wallet.credits += txn.amount
            @source_wallet.balance += txn.amount
            @source_wallet.balance -= txn.amount
            @source_wallet.debits += txn.amount
            @source_wallet.credits -= txn.amount
          end          
        elsif txn.withdrawal?
          txn.description = "Withdrawn to #{transaction['DestinationName']}"
          if txn.pending? || txn.processed?
            txn.txn_type = 'debit'
            @source_wallet.debits += txn.amount            
            @source_wallet.balance -= txn.amount
            @source_wallet.avail_balance = @source_wallet.balance
          elsif txn.failed?
            txn.txn_type = 'credit'
            @source_wallet.debits += txn.amount            
            @source_wallet.balance -= txn.amount
            @source_wallet.credits += txn.amount
            @source_wallet.debits -= txn.amount
            @source_wallet.balance += txn.amount
            @source_wallet.avail_balance = @source_wallet.balance
          end
        elsif txn.money_sent? || txn.fee?
          txn.description = "Sent to #{transaction['DestinationName']}" if txn.money_sent?
          txn.description = "Transaction fee charged" if txn.fee?
          txn.txn_type = "debit"
          @source_wallet.debits += txn.amount
          @source_wallet.balance -= txn.amount
          @source_wallet.avail_balance -= txn.amount
        elsif txn.money_received?
          txn.description = "Received from #{transaction['SourceName']}"
          txn.txn_type = "credit"
          @source_wallet.credits += txn.amount
          @source_wallet.balance += txn.amount
          @source_wallet.avail_balance += txn.amount
        end
        txn.balance = @source_wallet.balance
        txn.avail_balance = @source_wallet.avail_balance
        txn.wallet_id = @source_wallet.id
        txn.save
      end
       
      def log_message(type, msg)
        case type
          when :notice
            logger.info msg if defined? logger
          when :alert
            logger.warn msg if defined? logger
        end  
      end
    
  end  
#end