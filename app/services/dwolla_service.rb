require 'dwolla_api/oauth_scope'
require 'dwolla_api/encryptable'

require 'rubygems'
require 'dwolla'
require 'celluloid'

class DwollaService
  
#  include Celluloid
  include DwollaApi::OAuthScope
  include DwollaApi::Encryptable
  
  def initialize(wallet)
    @wallet = wallet
    Dwolla::api_key = ENV['DWOLLA_API_KEY']
    Dwolla::api_secret = ENV['DWOLLA_API_SECRET']
    Dwolla::sandbox=ENV['DWOLLA_SANDBOX']
    Dwolla::debug=false
    refresh_access_token_to_wallet if !@wallet.token.blank?
  end
  
end
