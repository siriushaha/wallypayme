module DwollaApi::TransactionScope
  
  def get_transaction(transaction_id, wallet=nil)
    begin
      #set_access_token wallet.token
      Dwolla::token = decrypt(wallet.token) unless wallet.nil?
      Dwolla::Transactions.get transaction_id        
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"
    end  
  end

  def get_transactions
    begin
      filters = { limit: 200 }
      Dwolla::Transactions.get filters      
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"
    end  
  end
    
end