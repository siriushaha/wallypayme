module DwollaApi::FundingScope
  
  def get_funding_sources
    begin
      Dwolla::FundingSources.get
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"
    end
  end
  
  def add_funding_sources_to_wallet
    begin
      funding_sources = get_funding_sources
      funding_sources.each { |source| save_funding_source source }
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"
    end
  end
  
  def get_details_by_funding_source(funding_source_id)
    begin 
      Dwolla::FundingSources.get(decrypt(funding_source_id))
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"
    end
  end
  
  def deposit_from(funding_source_id, options)
    begin
      Dwolla::FundingSources.deposit(decrypt(funding_source_id), options)
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}" 
    end
  end
  
  def withdraw_to(funding_source_id, options)
    begin
      Dwolla::FundingSources.withdraw(decrypt(funding_source_id), options)
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"       
    end
  end
  
  def add_funding_source_to_wallet(routing_no, acct_no, acct_type, acct_name)
    funding_source = { routing_number: routing_no, account_number: acct_no, account_type: acct_type, name: acct_name}
    begin
      source = Dwolla::FundingSources.add(funding_source)
      save_funding_source source
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"        
    end
  end
  
  def verify_funding_source_to_wallet(funding_source_id, deposit1, deposit2)
    begin
      source = Dwolla::FundingSources.verify(decrypt(funding_source_id), {deposit1: deposit1, deposit2: deposit2})
      funding_source = FundingSource.find_by(source_id: funding_source_id)
      unless funding_source.nil?
        funding_source.verified = source["Verified"]
        funding_source.save
      end
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"        
    end
  end
  
  private
  
  def save_funding_source(source)
    funding_source = FundingSource.new
    funding_source.source_id = encrypt source["Id"]
    funding_source.name = source["Name"]
    funding_source.acct_type = source["Type"]
    funding_source.verified = source["Verified"]
    funding_source.processing_type = source["ProcessingType"]
    funding_source.wallet_id = @wallet.id
    funding_source.save!
  end 

end
