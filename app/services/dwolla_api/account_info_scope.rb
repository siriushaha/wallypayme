module DwollaApi::AccountInfoScope
  
  def get_basic_account_info(id, required_to_save=false)
    begin
      basic_account_info = Dwolla::Users.get(id)
      save_account_info basic_account_info, required_to_save
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"
    end
  end
  
  def get_full_account_info(required_to_save=true)
    begin
      full_account_info = Dwolla::Users.get
      save_account_info full_account_info, required_to_save
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"
    end
  end
  
  private
  
  def save_account_info(account_info, required_to_save)
    @wallet.name = account_info['Name']
    @wallet.acct_id = encrypt account_info['Id']
    #@wallet.acct_type = Wallet.acct_types[account_info['Type'].to_sym]
    @wallet.longitude = account_info['Longitude'] 
    @wallet.latitude = account_info['Latitude'] 
    @wallet.acct_type = account_info['Type'] if account_info['Type'].present?
    @wallet.city = account_info['City'] if account_info['City'].present?
    @wallet.state = account_info['State'] if account_info['State'].present?
    @wallet.save if required_to_save
    @wallet
  end
  
end