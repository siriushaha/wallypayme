module DwollaApi::Encryptable
  extend ActiveSupport::Concern
  #included do
    PASSWORD = ENV["SECRET_KEY_BASE"]
  #end
  
  module ClassMethods
    def encrypt(field)
      AESCrypt.encrypt(field, PASSWORD)    
    end
  
    def decrypt(field)
    AESCrypt.decrypt(field, PASSWORD)
    end
  end
  
  private
  
  def decrypt(field)
    AESCrypt.decrypt(field, PASSWORD)
  end
  
  def encrypt(field)
    AESCrypt.encrypt(field, PASSWORD)
  end

#  def password
#    ENV["SECRET_KEY_BASE"]
#  end
  
end