module DwollaApi::OAuthScope
  def getOAuth(redirect_uri)
    scope = "send|balance|funding|transactions|accountinfofull"
    authUrl = Dwolla::OAuth.get_auth_url(redirect_uri, scope)    
  end
  
  def store_tokens_to_wallet(code, redirect_uri)
    if @wallet.token.blank? 
      token_info = Dwolla::OAuth.get_token(code, redirect_uri)
      save_tokens_to_wallet token_info
    end
  end

  def refresh_access_token_to_wallet
    token_info = Dwolla::OAuth.refresh_auth(decrypt(@wallet.refresh_token))
    save_tokens_to_wallet token_info
  end

  def set_access_token(token)
    Dwolla::token = decrypt(token) if !token.nil?
  end  

  private
    
  def save_tokens_to_wallet(token_info)
    @wallet.token = encrypt token_info['access_token']
    @wallet.refresh_token = encrypt token_info['refresh_token']
    @wallet.save!
    set_access_token @wallet.token
  end
  
end