module DwollaApi::BalanceScope
  def balance
    begin
      Dwolla::Balance.get
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"
    end
  end  
end
