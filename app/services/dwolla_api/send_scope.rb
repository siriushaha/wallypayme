module DwollaApi::SendScope
  def send(amount, notes, recipient_id, pin)
    begin
      transactionId = Dwolla::Transactions.send({ destinationId: decrypt(recipient_id), amount: amount, pin: pin, notes: notes})
    rescue => e
      raise DwollaServiceException, "Error: #{e.message}"
    end
  end
end