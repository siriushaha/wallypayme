class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :timeoutable, :timeout_in => 15.minutes 

  validates :name, presence: true
  
  has_one :wallet, dependent: :destroy
  
  default_scope { order(created_at: :desc) }
  scope :excluding_user_by_email, -> (user_email) { where.not(email: user_email)  } 
end
