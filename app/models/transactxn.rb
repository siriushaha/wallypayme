class Transactxn < ActiveRecord::Base
  enum txn_type: { debit: 1, credit: 2}
  enum status: { pending: 1, processed: 2, failed: 3, cancelled: 4, reclaimed: 5}
  enum txn_is_a_type_of: { money_sent: 1, money_received: 2, deposit: 3, withdrawal: 4, fee: 5 }
  
  belongs_to :wallet
  
  scope :by_type, -> (type) { where(txn_type: type)}
  #scope :by_debit_type, -> { where(txn_type: Transactxn.txn_types[:debit]) }
  #scope :by_credit_type, -> { where(txn_type: Transactxn.txn_types[:credit]) }
  scope :from_date, -> (from_date) { where('created_at >= ?', from_date) }
  scope :to_date, -> (to_date) { where('created_at <= ?', to_date) }
  scope :between_dates, -> (from_date, to_date) { where('created_at between ? and ?', from_date, to_date) }
#  scope :by_pending_deposits, -> { where(status: Transactxn.statuses[:pending], txn_is_a_type_of: Transactxn.txn_is_a_type_ofs[:deposit]) }
#  scope :by_not_pending_deposits, -> { where.not(status: Transactxn.statuses[:pending], txn_is_a_type_of: Transactxn.txn_is_a_type_ofs[:deposit]) }
  scope :by_pending_txns, -> { where(status: Transactxn.statuses[:pending]) }
  scope :by_not_pending_txns, -> { where.not(status: Transactxn.statuses[:pending]) }
  default_scope { order(updated_at: :desc) }
  
  validates :amount, numericality: {
    greater_than: 0,
    message: "can only be positive greater than 0"
    }
 
  def debit_amount
    amount if self.debit?
  end
  
  def credit_amount
    amount if self.credit?
  end
  
 end
