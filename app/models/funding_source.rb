class FundingSource < ActiveRecord::Base
  belongs_to :wallet
  
  validates_presence_of :source_id, :name
  #validates_uniqueness_of :name
  
  scope :by_external_accounts, -> { where.not(name: 'My Dwolla Balance') }
  scope :verified, -> { where(verified: true).order(name: :asc) }
  
end
