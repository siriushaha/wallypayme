class Wallet < ActiveRecord::Base
  enum acct_type: {Personal: 1, Commercial: 2}
  
  belongs_to :user
  has_many :transactxns, dependent: :destroy
  has_many :funding_sources, dependent: :destroy
  
  validates :balance, :credits, :debits, numericality: {
    greater_than_or_equal_to: 0,
    message: "can only be positive greater than 0"
    }
  
end
