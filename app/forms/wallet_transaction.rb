class WalletTransaction
  include ActiveModel::Model
 
  attr_accessor :amount, :decimal
  attr_accessor :pin, :string
  
  validates_presence_of :amount, :pin
  validates :pin, length: { is: 4 }
  validates :amount, numericality: {
    greater_than: 0.0,
    message: "can only be positive greater than 0"
  }
  
end
