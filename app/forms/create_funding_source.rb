class CreateFundingSource
  include ActiveModel::Model
  attr_accessor :name, :string
  attr_accessor :type, :string
  attr_accessor :routing_number, :string
  attr_accessor :account_number, :string
  
  validates :name, :type, presence: true
  validates :routing_number, presence: true, length: { is: 9 }
  validates :account_number, presence: true

end