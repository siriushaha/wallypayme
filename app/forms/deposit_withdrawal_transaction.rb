class DepositWithdrawalTransaction < WalletTransaction
  attr_accessor :funding_source, :integer
  validates_presence_of :funding_source    
end
