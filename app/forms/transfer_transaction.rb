class TransferTransaction < WalletTransaction
  attr_accessor :notes, :text
  attr_accessor :target_email, :string 

  validates_presence_of :target_email
  validates_format_of :target_email, :with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/
  
end