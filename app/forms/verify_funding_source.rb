class VerifyFundingSource
  include ActiveModel::Model

  attr_accessor :deposit1, :decimal
  attr_accessor :deposit2, :decimal
  
  validates :deposit1, :deposit2, presence: true
  validates :deposit1, :deposit2, numericality: {
    greater_than: 0.0,
    less_than: 0.15,
    message: "must be between $0.01 and $0.14"
  }
end