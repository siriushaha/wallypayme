class AddWalletRefToTransactions < ActiveRecord::Migration
  def change
    add_reference :transactions, :wallet, index: true
  end
end
