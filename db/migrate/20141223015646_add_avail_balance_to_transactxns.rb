class AddAvailBalanceToTransactxns < ActiveRecord::Migration
  def change
    add_column :transactxns, :avail_balance, :decimal, precision: 10, scale: 2, default: 0.0
  end
end
