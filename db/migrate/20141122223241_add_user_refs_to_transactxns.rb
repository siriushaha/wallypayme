class AddUserRefsToTransactxns < ActiveRecord::Migration
  def change
    add_column :transactxns, :source_id, :integer
    add_column :transactxns, :target_id, :integer
  end
end
