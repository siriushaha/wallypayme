class RenameTransactionIdToTransactxns < ActiveRecord::Migration
  def change
    rename_column :transactxns, :transaction_id, :reference_no
  end
end
