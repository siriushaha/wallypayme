class AddWalletRefToTransactxns < ActiveRecord::Migration
  def change
    add_reference :transactxns, :wallet, index: true
  end
end
