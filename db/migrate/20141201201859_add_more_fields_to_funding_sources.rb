class AddMoreFieldsToFundingSources < ActiveRecord::Migration
  def change
    add_column :funding_sources, :acct_type, :string
    add_column :funding_sources, :verified, :boolean
    add_column :funding_sources, :processing_type, :string
  end
end
