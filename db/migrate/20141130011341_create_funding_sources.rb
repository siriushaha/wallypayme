class CreateFundingSources < ActiveRecord::Migration
  def change
    create_table :funding_sources do |t|
      t.string :name
      t.references :wallet, index: true

      t.timestamps
    end
  end
end
