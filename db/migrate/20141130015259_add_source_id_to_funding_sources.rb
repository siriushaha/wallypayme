class AddSourceIdToFundingSources < ActiveRecord::Migration
  def change
    add_column :funding_sources, :source_id, :string
  end
end
