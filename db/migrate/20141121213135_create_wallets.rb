class CreateWallets < ActiveRecord::Migration
  def change
    create_table :wallets do |t|
      t.text :description
      t.decimal :balance, scale: 2, precision: 10, default: 0.0
      t.decimal :credits, scale: 2, precision: 10, default: 0.0
      t.decimal :debits, scale: 2, precision: 10, default: 0.0

      t.timestamps
    end
  end
end
