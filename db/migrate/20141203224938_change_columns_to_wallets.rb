class ChangeColumnsToWallets < ActiveRecord::Migration
  def change
    change_column :wallets, :longitude, :decimal, precision: 10, scale: 7
    change_column :wallets, :latitude, :decimal, precision: 10, scale: 7
    change_column :wallets, :description, :string
  end
end
