class AddAvailableBalanceToWallet < ActiveRecord::Migration
  def change
    add_column :wallets, :avail_balance, :decimal, precision: 10, scale: 2, default: 0.0
  end
end
