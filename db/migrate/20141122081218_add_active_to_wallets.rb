class AddActiveToWallets < ActiveRecord::Migration
  def change
    add_column :wallets, :active, :boolean, default: true
  end
end
