class AddClearingDateToTransactxns < ActiveRecord::Migration
  def change
    add_column :transactxns, :clearing_date, :datetime
  end
end
