class RenameTypeFieldInTransactxns < ActiveRecord::Migration
  def change
    rename_column :transactxns, :type, :txn_type
  end
end
