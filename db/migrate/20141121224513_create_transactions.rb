class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :type
      t.decimal :amount
      t.decimal :balance
      t.string :description

      t.timestamps
    end
  end
end
