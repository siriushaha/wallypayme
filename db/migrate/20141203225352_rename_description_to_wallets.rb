class RenameDescriptionToWallets < ActiveRecord::Migration
  def change
    rename_column :wallets, :description, :name
  end
end
