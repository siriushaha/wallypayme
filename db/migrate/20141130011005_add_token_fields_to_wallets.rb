class AddTokenFieldsToWallets < ActiveRecord::Migration
  def change
    add_column :wallets, :token, :string
    add_column :wallets, :refresh_token, :string
  end
end
