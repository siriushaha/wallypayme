class AddAccountInfoToWallets < ActiveRecord::Migration
  def change
    add_column :wallets, :acct_id, :string
    add_column :wallets, :acct_type, :integer
    add_column :wallets, :longitude, :decimal
    add_column :wallets, :latitude, :decimal
    add_column :wallets, :city, :string
    add_column :wallets, :state, :string
  end
end
