class AddDefaultsToTransactions < ActiveRecord::Migration
  def change
    change_column :transactions, :amount, :decimal,  scale: 2, precision: 10, default:0.0
    change_column :transactions, :balance, :decimal,  scale: 2, precision: 10, default:0.0
  end
end
