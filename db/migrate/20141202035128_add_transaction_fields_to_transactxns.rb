class AddTransactionFieldsToTransactxns < ActiveRecord::Migration
  def change
    add_column :transactxns, :transaction_id, :string
    add_column :transactxns, :status, :integer
    add_column :transactxns, :txn_is_a_type_of, :integer
  end
end
