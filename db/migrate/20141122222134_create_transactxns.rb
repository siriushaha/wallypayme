class CreateTransactxns < ActiveRecord::Migration
  def change
    create_table :transactxns do |t|
      t.integer :type
      t.decimal :amount, scale: 2, precision: 10, default: 0.0
      t.decimal :balance, scale: 2, precision: 10, default: 0.0
      t.string :description

      t.timestamps
    end
  end
end
