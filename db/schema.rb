# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141223015646) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "funding_sources", force: :cascade do |t|
    t.string   "name"
    t.integer  "wallet_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "source_id"
    t.string   "acct_type"
    t.boolean  "verified"
    t.string   "processing_type"
  end

  add_index "funding_sources", ["wallet_id"], name: "index_funding_sources_on_wallet_id", using: :btree

  create_table "transactxns", force: :cascade do |t|
    t.integer  "txn_type"
    t.decimal  "amount",           precision: 10, scale: 2, default: 0.0
    t.decimal  "balance",          precision: 10, scale: 2, default: 0.0
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "wallet_id"
    t.integer  "source_id"
    t.integer  "target_id"
    t.string   "reference_no"
    t.integer  "status"
    t.integer  "txn_is_a_type_of"
    t.text     "notes"
    t.datetime "clearing_date"
    t.decimal  "avail_balance",    precision: 10, scale: 2, default: 0.0
  end

  add_index "transactxns", ["wallet_id"], name: "index_transactxns_on_wallet_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.boolean  "admin",                  default: false
    t.boolean  "active",                 default: true
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "wallets", force: :cascade do |t|
    t.string   "name"
    t.decimal  "balance",       precision: 10, scale: 2, default: 0.0
    t.decimal  "credits",       precision: 10, scale: 2, default: 0.0
    t.decimal  "debits",        precision: 10, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.boolean  "active",                                 default: true
    t.string   "token"
    t.string   "refresh_token"
    t.string   "acct_id"
    t.integer  "acct_type"
    t.decimal  "longitude",     precision: 10, scale: 7
    t.decimal  "latitude",      precision: 10, scale: 7
    t.string   "city"
    t.string   "state"
    t.decimal  "avail_balance", precision: 10, scale: 2, default: 0.0
  end

  add_index "wallets", ["user_id"], name: "index_wallets_on_user_id", using: :btree

end
