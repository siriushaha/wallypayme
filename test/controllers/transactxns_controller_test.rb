require 'test_helper'

class TransactxnsControllerTest < ActionController::TestCase
  setup do
    @transactxn = transactxns(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transactxns)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transactxn" do
    assert_difference('Transactxn.count') do
      post :create, transactxn: { amount: @transactxn.amount, amount: @transactxn.amount, balance: @transactxn.balance, balance: @transactxn.balance, description: @transactxn.description, type: @transactxn.type }
    end

    assert_redirected_to transactxn_path(assigns(:transactxn))
  end

  test "should show transactxn" do
    get :show, id: @transactxn
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transactxn
    assert_response :success
  end

  test "should update transactxn" do
    patch :update, id: @transactxn, transactxn: { amount: @transactxn.amount, amount: @transactxn.amount, balance: @transactxn.balance, balance: @transactxn.balance, description: @transactxn.description, type: @transactxn.type }
    assert_redirected_to transactxn_path(assigns(:transactxn))
  end

  test "should destroy transactxn" do
    assert_difference('Transactxn.count', -1) do
      delete :destroy, id: @transactxn
    end

    assert_redirected_to transactxns_path
  end
end
